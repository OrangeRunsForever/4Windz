﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public Text currPlayer;
    public GameObject winScreenBGR, winScreenContent;
    public Text winScreenTitle, winScreenPlayerNo;

    public void ChangePlayer(int playerNo)
    {
        if (playerNo == 1)
        {
            currPlayer.text = "one";
            currPlayer.color = Color.green;
        }
        else
        {
            currPlayer.text = "two";
            currPlayer.color = Color.red;
        }
    }

    public void ShowWinScreen(string PlayerNo)
    {
        //deactivate test showing the current players turn
        currPlayer.transform.parent.gameObject.SetActive(false);

        //show this only if it's a draw
        if (PlayerNo == "nobody won!")
        {
            winScreenTitle.gameObject.SetActive(false);
        }

        winScreenPlayerNo.text = "Congratulations \n" + PlayerNo;

        winScreenBGR.SetActive(true);
        winScreenContent.SetActive(true);
    }

    public void RestartCurrentLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


}
