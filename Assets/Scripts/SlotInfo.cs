﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotInfo : MonoBehaviour
{
    public bool isFull = false;
    public int indexX, indexY, indexZ;
    public int playerNoPiece = 0;

}
