﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private float camXRot;
    private HighlightsFX outlineScript;

    public Transform gameboard;
    public bool mouseIsOverColumn = false;
    public Transform selectedColumn;

    // Use this for initialization
    void Start ()
    {
        outlineScript = GetComponent<HighlightsFX>();
	}

    // Update is called once per frame
    void Update()
    {
        camXRot = Camera.main.transform.rotation.x;

        RaycastHit hit;

        gameboard.tag = "Untagged";
        outlineScript.UpdateOccluder();

        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (hit.transform.name == "Bottom")
            {
                gameboard.tag = "Occluder";
                outlineScript.UpdateOccluder();
            }
        }


        /*
        if(camXRot <= 0)
        {
            gameboard.tag = "Occluder";
            outlineScript.UpdateOccluder();
        }
        else
        {
            gameboard.tag = "Untagged";
            outlineScript.UpdateOccluder();
        }
        */

        MouseHoverOver();
	}

    void MouseHoverOver()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast (ray, out hit, 100F))
        {
            if(hit.transform.tag == "Columns")
            {
                mouseIsOverColumn = true;
                outlineScript.enabled = true;
                outlineScript.objectRenderer = hit.transform.GetComponent<Renderer>();
                selectedColumn = hit.transform;
            }
            else
            {
                mouseIsOverColumn = false;
                outlineScript.enabled = false;
            }
        }
        else
        {
            mouseIsOverColumn = false;
            outlineScript.enabled = false;
        }
    }


}
