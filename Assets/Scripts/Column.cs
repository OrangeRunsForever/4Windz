﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column : MonoBehaviour
{
    public List<Transform> slots = new List<Transform>();

    public bool colIsFull = false;
    public Transform colEntryPos;
    // Use this for initialization

    public void InitColumn()
    {
        //iterate through all children of this column transform
        //and add them to the slots lits
        foreach (Transform slot in GetComponentsInChildren<Transform>())
        {
            if (slot != this.transform && slot.name == "slot")
            {
                slots.Add(slot);
                slot.gameObject.AddComponent<SlotInfo>();
            }
        }
    }

    //this is being called after a new piece was spawned
    public void CheckIfFull()
    {

        int counter = 0;

        foreach (Transform slot in slots)
        {
            if (slot.GetComponent<SlotInfo>().isFull)
            {
                counter++;
            }
   
        }

        //if it only has one slot left
        //then the just spawned piece fills it up
        if(counter >= 4)
        {
            colIsFull = true;
        }
        else
        {
            colIsFull = false;
        }
    }



}
