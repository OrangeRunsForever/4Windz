﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PostProcessing;

public enum Winner { none, player1, player2, draw}

public class GameController : MonoBehaviour
{
    public static GameController gameC;
    
    public CameraController camCont;
    public GameObject[] playingPieces;
    public Transform playingBoard;
    public Transform endPosForegroundBoard;
    public PostProcessingProfile m_Profile;

    private Winner winType;

    private BoardController board;
    private UIController UIC;
    private AudioController AC;
    private GameObject[,,] slotsGrid = new GameObject[4, 4, 4];

    public bool isLerping;

    private int currPlayer = 0;

    private void Awake()
    {
        board = playingBoard.GetComponent<BoardController>();
        playingBoard.GetComponent<MouseRotateObject>().enabled = true;
        UIC = FindObjectOfType<UIController>();
        AC = FindObjectOfType<AudioController>();

        //SINGLETON
        if (gameC == null)
        {
            gameC = this;
        }

        else
        {
            Destroy(this.gameObject);
        }
    }

    //Init
    private void Start()
    {
        //init every column at start
        foreach(GameObject col in board.columns)
        {
            col.GetComponent<Column>().InitColumn();
        }

        InitGrid();
        AC.InitAudio();
        camCont.enabled = true;
    }

    private void InitGrid()
    {
        //initial save all columns from boardC
        //in our 3D grid (the columns are the top in X and Z)
        //(Y will be the depth/slots in each column later)
        int counter = 0;
        for (int z = 0; z < 4; z++)
        {
            for (int x = 0; x < 4; x++)
            {
                int y = 0;
                foreach (Transform slot in board.columns[counter].GetComponent<Column>().slots)
                {
                    slotsGrid[x, y, z] = slot.gameObject;

                    SlotInfo stats = slot.gameObject.GetComponent<SlotInfo>();

                    stats.indexX = x;
                    stats.indexY = y;
                    stats.indexZ = z;

                    y++;
                }

                counter++;
            }

        }

        currPlayer = 1;
        UIC.ChangePlayer(currPlayer);
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if (camCont.mouseIsOverColumn && !isLerping)
            {
                SpawnPiece(camCont.selectedColumn);
            }
        }

    }

    private Winner CheckForWin(SlotInfo slot)
    {
        //slot.isFull = true;
        slot.playerNoPiece = currPlayer;

        winType = Winner.none;

        for(int x=0; x<4; x++)
        {
            for (int y=0; y<4; y++)
            {
                for (int z=0; z<4; z++)
                {
                    if(CheckPos(x, y, z))
                    {
                        if(slotsGrid[x,y,z].GetComponent<SlotInfo>().playerNoPiece == 1)
                        {
                            if (winType == Winner.player2)
                            {
                                winType = Winner.draw;
                            }
                            else
                            {
                                winType = Winner.player1;
                            }

                        }
                        else if(slotsGrid[x, y, z].GetComponent<SlotInfo>().playerNoPiece == 2)
                        {
                            if (winType == Winner.player1)
                            {
                                winType = Winner.draw;
                            }
                            else
                            {
                                winType = Winner.player2;
                            }
                        }

                    }
                    //if there was no winner
                    //and no immediate draw (cause of other effects for instance)
                    //then check if there are still empty slots
                    else
                    {
                        int counter = 0;

                        foreach (GameObject slotToCheck in slotsGrid)
                        {
                            //if he finds any slot that is still empty
                            //abort
                            if(slotToCheck.GetComponent<SlotInfo>().isFull)
                            {
                                counter++;
                            }
                        }

                        if (counter == slotsGrid.Length)
                        {
                            return Winner.draw;
                        }
                    }
                }
            }
        }

        return winType;
    }

    bool CheckPos(int x, int y, int z)
    {
        if(!slotsGrid[x,y,z].GetComponent<SlotInfo>().isFull)
        {
            return false;
        }
        return 
               //X,Y,Z
               CheckPos(x, y, z, 1, 0, 0) ||
               CheckPos(x, y, z, 0, 1, 0) ||
               CheckPos(x, y, z, 0, 0, 1) ||
               //X,Y
               CheckPos(x, y, z, 1, 1, 0) ||
               CheckPos(x, y, z, 1, -1, 0) ||
               //X,Z
               CheckPos(x, y, z, 1, 0, 1) ||
               CheckPos(x, y, z, 1, 0, -1) ||
               //Y,Z
               CheckPos(x, y, z, 0, 1, 1) ||
               CheckPos(x, y, z, 0, 1, -1) ||
               //X,Y,Z
               CheckPos(x, y, z, 1, 1, 1) ||
               CheckPos(x, y, z, 1, -1, -1) ||
               CheckPos(x, y, z, 1, 1, -1) ||
               CheckPos(x, y, z, 1, -1, -1);
    }

    bool CheckPos(int x, int y, int z, int dx, int dy, int dz)
    {
        int n = 0;
        int playerNo = slotsGrid[x, y, z].GetComponent<SlotInfo>().playerNoPiece;
        while (0 <= x && 0 <= y && 0 <= z && x < 4 && y < 4 && z < 4 && slotsGrid[x, y, z].GetComponent<SlotInfo>().playerNoPiece == playerNo)
        {
            n++;

            x += dx; 
            y += dy;
            z += dz;
        }

        return n >= 4;
    }

    private void SpawnPiece(Transform column)
    {
        if (!column.GetComponent<Column>().colIsFull)
        {
            //Vector3 posToSpawn = new Vector3(column.position.x, column.position.y + (column.GetComponent<Renderer>().bounds.extents.y * 1.8F), column.position.z * 0.990F);
            Vector3 posToSpawn = column.GetComponent<Column>().colEntryPos.position;

            GameObject newPlayingPiece = Instantiate(playingPieces[currPlayer-1], posToSpawn, column.rotation);
            newPlayingPiece.transform.name = "ActivePlayingPiece";
            newPlayingPiece.transform.parent = column;

            FindSlot(column, newPlayingPiece);

        }
    }

    void UpdateGrid(Transform column, Transform slot)
    {
        foreach(GameObject space in slotsGrid)
        {
            if(space.transform == slot)
            {
                switch(CheckForWin(space.GetComponent<SlotInfo>()))
                {
                    case Winner.draw:
                        PlayerWon(0);
                        break;
                    case Winner.player1:
                        PlayerWon(1);
                        break;
                    case Winner.player2:
                        PlayerWon(2);
                        break;
                    case Winner.none:
                        break;
                }
            }
        }
    }

    private void FindSlot(Transform column, GameObject newPlayingPiece)
    {
        Column currCol = column.GetComponent<Column>();

        foreach(Transform slot in currCol.slots)
        {
            //if first slot found in column is empty
            if(!slot.GetComponent<SlotInfo>().isFull)
            {
                //lerp playingPiece there
                StartCoroutine(StartLerpToSlot(slot, newPlayingPiece));

                //set the found empty slot to full
                slot.GetComponent<SlotInfo>().isFull = true;

                //tell the column to check if it's full
                currCol.CheckIfFull();
                return;
            }
        }
    }

    void PlayerWon(int playerNo)
    {
        string tempString = "";

        switch(playerNo)
        {
            case 0:
                tempString = "nobody won";
                break;
            case 1:
                tempString = "player one";
                break;
            case 2:
                tempString = "player two";
                break;
        }

        StartCoroutine(LerpModelToForeground());


        UIC.ShowWinScreen(tempString);

        //change postProSettings
        //m_Profile.depthOfField.enabled = false;
        var tempSettings = m_Profile.depthOfField.settings;
        tempSettings.focusDistance = 0.13F;
        tempSettings.aperture = 3.9F;
        tempSettings.focalLength = 11F;

        m_Profile.depthOfField.settings = tempSettings;

        //deactivate things not needed after win
        camCont.mouseIsOverColumn = false;
        camCont.enabled = false;
        playingBoard.GetComponent<MouseRotateObject>().enabled = false;

        //but activate the rotate object when mouse over
        playingBoard.GetComponent<BoxCollider>().enabled = true;
        playingBoard.gameObject.AddComponent<StartRotateOnHoverOver>();
        playingBoard.parent = Camera.main.transform;

        
    }

    IEnumerator LerpModelToForeground()
    {
        playingBoard.localScale = new Vector3(0.15F, 0.15F, 0.15F);
        /*
        while (!V3Equal(playingBoard.localScale, new Vector3(0.15F, 0.15F, 0.15F)))
        {
            playingBoard.localScale = Vector3.Lerp(playingBoard.localScale, new Vector3(0.15F, 0.15F, 0.15F), 1000F * Time.smoothDeltaTime);

            yield return null;
        }
        */

        while (!V3Equal(playingBoard.position, endPosForegroundBoard.position))
        {
            playingBoard.position = Vector3.Lerp(playingBoard.position, endPosForegroundBoard.position, 10F * Time.smoothDeltaTime);

            yield return null;
        }



        yield return null;
    }

    void SwitchPlayer()
    {
        if(currPlayer == 1)
        {
            currPlayer = 2;
        }
        else if(currPlayer == 2)
        {
            currPlayer = 1;
        }

        UIC.ChangePlayer(currPlayer);
    }

    IEnumerator StartLerpToSlot(Transform slot,GameObject newPlayingPiece)
    {
        isLerping = true;

        Vector3 wantedPos = slot.position;

        while(!V3Equal(newPlayingPiece.transform.position, wantedPos))
        {
            newPlayingPiece.transform.position = Vector3.Lerp(newPlayingPiece.transform.position, wantedPos, Time.deltaTime * 5F);
            yield return null;
        }

        //update our slotGrid and check for win
        UpdateGrid(slot.parent, slot);

        //set corresponding deactivated (full) slot
        //as child to it's playing piece
        newPlayingPiece.transform.parent = slot.transform;



        isLerping = false;

        SwitchPlayer();
        
        yield return null;
    }

    public bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0000025;
    }
}
