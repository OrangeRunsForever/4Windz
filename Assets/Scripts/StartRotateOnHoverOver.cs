﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StartRotateOnHoverOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    bool mouseIsover = false;

    public int turnSpeed = 10;


    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseIsover = true;
        bool isUI = false;

        if(this.gameObject.layer == 5) //layer 5 == UI
        {
            isUI = true;
        }

        StartCoroutine(RotateObject(isUI));
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseIsover = false;
    }

    IEnumerator RotateObject(bool isUI)
    {
        if (isUI)
        {
            while (mouseIsover)
            {
                transform.Rotate(new Vector3(0, 0, 5F * Time.smoothDeltaTime * turnSpeed));
                yield return null;
            }
        }
        else
        {
            while (mouseIsover)
            {
                transform.Rotate(new Vector3(0, 5F * Time.smoothDeltaTime * turnSpeed, 0));
                yield return null;
            }
        }

        yield return null;

    }
}
