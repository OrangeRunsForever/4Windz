﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLookAtMouse : MonoBehaviour {

    public float rotationSpeed = 8;  //This will determine max rotation speed, you can adjust in the inspector

    public Camera cam;  //Drag the camera object here

    void Update()
    {
        //If you want to prevent rotation, just don't call this method

        RotateObject();
    }

    void RotateObject()
    {
        //Get mouse position
        Vector3 mousePos = Input.mousePosition;

        //Adjust mouse z position
        mousePos.z = cam.transform.position.y - transform.position.y;

        //Get a world position for the mouse
        Vector3 mouseWorldPos = cam.ScreenToWorldPoint(mousePos);

        //Get the angle to rotate and rotate
        float angle = -Mathf.Atan2(transform.position.z - mouseWorldPos.z, transform.position.x - mouseWorldPos.x) * Mathf.Rad2Deg;
        //float angle = Vector3.Angle(transform.position, mouseWorldPos);

        transform.localRotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, angle, 0), rotationSpeed * Time.deltaTime);

    }
}