﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour
{
    public int maxAngleY = 45;
    public int minAngleY = -45;



    Vector2 clickPos;
    Vector2 offsetPos;
    int divider = 50;
    Vector2 lastPosMouse;

    void Start()
    {
        clickPos = new Vector2(0, 0);
        offsetPos = new Vector2(0, 0);
    }

    void Update()
    {
        if (lastPosMouse != mouseXY())
        {
            offsetPos = new Vector2(0, 0);

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                clickPos = mouseXY();
            }

            if (Input.GetKey(KeyCode.Mouse0))
            {
                offsetPos = clickPos - mouseXY();
            }



            // Rotate the GameObject
            transform.Rotate(new Vector3(-(offsetPos.y / divider), offsetPos.x / divider, 0.0f), Space.World);

            transform.rotation = Quaternion.Euler(new Vector3(Mathf.Clamp(Angle(transform.rotation.eulerAngles.x), minAngleY, maxAngleY), 
                                                                                transform.rotation.eulerAngles.y, 
                                                                                Mathf.Clamp(Angle(transform.rotation.eulerAngles.z), minAngleY, maxAngleY)));
            
        }
        else
        {
            clickPos = mouseXY();
        }
    }

    void LateUpdate()
    {
        lastPosMouse = mouseXY();
    }

    // Return true when left mouse is clicked or hold
    KeyCode leftClick()
    {
        return KeyCode.Mouse0;
    }

    //Immediate location of the mouse
    Vector2 mouseXY()
    {
        return new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    }

    //Immediate location of the mouse's X coordinate
    float mouseX()
    {
        return Input.mousePosition.x;
    }

    //Immediate location of the mouse's Y coordinate
    float mouseY()
    {
        return Input.mousePosition.y;
    }

    public float Angle(float angle)
    {

        if (angle < 180)
            return angle;
        if (angle > 180)
            angle = angle - 360F;

        return angle;
            
        
    }
}